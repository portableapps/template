# PortableApps.com Development

The PortableApps.com Development section is designed to provide resources to developers working with portable applications - both adaptations of existing apps as well as original apps designed to be portable.

## This repository

For development, this branch will be mainteain with the last version of [PortableApps.com Application Template](https://sourceforge.net/projects/portableapps/files/PortableApps.com%20Template/)